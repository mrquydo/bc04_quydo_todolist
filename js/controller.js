renderDSTASK = function (dstask) {
  var contentHTML = "";

  dstask.forEach((task) => {
    var content = `
      <li>
      <span>${task.des}</span>
      <span>
      <i class="fa fa-trash-alt" onclick=xoaTask('${task.id}')></i>
      <i class="fa fa-check-circle" onclick=doneTask('${task.id}') style="margin-left: 10px"></i>
      </span>
      </li>`;
    contentHTML += content;
  });

  document.getElementById("todo").innerHTML = contentHTML;
};

renderDSDONE = function (dstask) {
  var contentHTML = "";

  dstask.forEach((task) => {
    var content = `
        <li>
        <span>${task.des}</span>
        <span>
        <i class="fa fa-trash-alt" onclick=xoaTask('${task.id}')></i>
        <i class="fa fa-check-circle" onclick=doneTask('${task.id}') style="margin-left: 10px"></i>
        </span>
        </li>`;
    contentHTML += content;
  });

  document.getElementById("completed").innerHTML = contentHTML;
};

function layThongTinNewTask() {
  let newTask = {
    des: document.getElementById("newTask").value,
  };
  return newTask;
}

function sortAToZ(dstask) {
  dstask.sort((a, b) => a.des - b.des);
}
