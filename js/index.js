const BASE_URL = "https://62db6ca4e56f6d82a7728511.mockapi.io";

let doneList = [];

function getDSTASK() {
  // batLoading();
  axios({
    url: `${BASE_URL}/task`,
    method: "GET",
  })
    .then(function (res) {
      // tatLoading();

      renderDSTASK(res.data);
    })
    .catch(function (err) {
      // tatLoading();
      console.log(err);
    });
}
getDSTASK();

function addTask() {
  let newTask = layThongTinNewTask();
  axios({
    url: `${BASE_URL}/task`,
    method: "POST",
    data: newTask,
  })
    .then(function (res) {
      getDSTASK();
      document.getElementById("newTask").value = "";
    })
    .catch(function (err) {
      console.log(err);
    });
}

function xoaTask(id) {
  axios({
    url: `${BASE_URL}/task/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      getDSTASK();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function doneTask(id) {
  axios({
    url: `${BASE_URL}/task/${id}`,
    method: "GET",
  })
    .then(function (res) {
      doneList.push(res.data);
      renderDSDONE(doneList);
      xoaTask(id);
    })
    .catch(function (err) {
      console.log(err);
    });
}
// Sort A > Z

document.getElementById("two").addEventListener("click", function () {
  axios({
    url: `${BASE_URL}/task`,
    method: "GET",
  })
    .then(function (res) {
      sortAToZ(res.data);
      console.log(res.data);
      renderDSTASK(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
});
